# app/assets/javascripts/components/RecordForm.js.coffee

RecordActions = require '../actions/RecordActions'

RecordForm = React.createClass(
  # Notice that the state here is for the form fields, not an actual Record object
  getInitialState: ->
    title: ''
    date: ''
    amount: ''

  # Same as the one in Record. Could probably be moved to utils.
  handleChange: (e) ->
    newState = {}
    newState[e.target.name] = e.target.value
    @setState newState

  # Event called to create a new Record
  handleSubmit: (e) ->
    e.preventDefault()
    # Send the data to the Rails server. Notice that we're submitting @state as the data for the
    # new record!
    $.post '', { record: @state }, (data) =>
      # After it's done, use an action to update the store, and then the view
      RecordActions.createRecord(data)
      # Clear the form
      @setState @getInitialState()
    , 'JSON'

  valid: ->
    @state.date && @state.title && @state.amount

  # Another form
  render: ->
    React.DOM.form
      className: 'form-inline'
      onSubmit: @handleSubmit
      React.DOM.div
        className: 'form-group'
        React.DOM.input
          type: 'text'
          className: 'form-control'
          placeholder: 'Date'
          name: 'date'
          value: @state.date
          onChange: @handleChange
      React.DOM.div
        className: 'form-group'
        React.DOM.input
          type: 'text'
          className: 'form-control'
          placeholder: 'Title'
          name: 'title'
          value: @state.title
          onChange: @handleChange
      React.DOM.div
        className: 'form-group'
        React.DOM.input
          type: 'number'
          className: 'form-control'
          placeholder: 'Amount'
          name: 'amount'
          value: @state.amount
          onChange: @handleChange
      React.DOM.button
        type: 'submit'
        className: 'btn btn-primary'
        disabled: !@valid()
        'Create Record'
)

module.exports = RecordForm