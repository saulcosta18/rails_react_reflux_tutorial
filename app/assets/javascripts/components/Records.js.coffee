# app/assets/javascripts/components/Records.js.coffee

# Pull in all the building blocks, some of which we'll create after this file
AmountBox     = require './AmountBox'
Record        = require './Record'
RecordActions = require '../actions/RecordActions'
RecordForm    = require './RecordForm'
RecordStore   = require '../stores/RecordStore'

# Create the React class. Notice that I'm using "winodw.Records" here. This might not be correct,
# but it seemed to work really well with Rails and Reflux.
window.Records = React.createClass

  # In React, you'll see a lot of two things: state and props. This sets the default value for
  # state.
  getInitialState: ->
    records: []

  # And this sets the default value for props.
  getDefaultProps: ->
    records: []

  # Whatever you put here will be called when the page is first loaded
  componentDidMount: ->
    # Load the current data from the Rails server
    $.get '/records.json', (data) =>
      RecordActions.setRecords(data)
    # Start listening for changes triggered by the store, and when something changes, call the
    # 'onChange' function
    @unsubscribe = RecordStore.listen(@onChange)

  # When we're done, stop listening to the store
  componentWillUnmount: ->
    @unsubscribe()

  # Remember that 'trigger' function in RecordStore? This is where _records go. 'setState' is a
  # special function we use to change the state of the page. This is the ONLY correct way to
  # change the state of a page.
  onChange: (records) ->
    # 'records' are just '_records' from RecordStore
    @setState records: records

  # This will be used to calculate the 'credits' box at the top of the page
  credits: ->
    # Using underscore, filter our records to ones that have a positive amount
    credits = _.filter @state.records, (r) -> r.amount >= 0
    # Sum them
    _.reduce credits, ((s, r) -> s + r.amount ), 0

  # For the 'debits' box at the top of the page
  debits: ->
    # Using underscore, filter our records to ones that have a negative amount
    debits = _.filter @state.records, (r) -> r.amount < 0
    # Sum them
    _.reduce debits, ((s, r) -> s + r.amount ), 0

  # Another box we'll have at the top of the page
  balance: ->
    @debits() + @credits()


  # 'render' is what you actually see on the page
  render: ->
    # Create a <div> with a class of 'records', etc.
    React.DOM.div
      className: 'records'
      React.DOM.h2
        className: 'title'
        'Records'
      React.DOM.div
        className: 'row'
        # Here we're creating other React elements. We haven't written these yet, but these will
        # make more sense when we have. For now, take note of the params we're passing in:
        # 'type', 'amount' and 'text'.
        React.createElement AmountBox, type: 'success', amount: @credits(), text: 'Credits'
        React.createElement AmountBox, type: 'danger',  amount: @debits(), text: 'Debits'
        React.createElement AmountBox, type: 'info', amount: @balance(), text: 'Balance'
      # Don't let this one slip by you! This is an entire form that we'll define in RecordForm.
      React.createElement RecordForm
      React.DOM.hr null
      # Create a table to store the current records
      React.DOM.table
        className: 'table table-bordered'
        # All these 'nulls' could instead be 'className: "myClass"' for example
        React.DOM.thead null,
          React.DOM.tr null,
            React.DOM.th null, 'Date'
            React.DOM.th null, 'Title'
            React.DOM.th null, 'Amount'
            React.DOM.th null, 'Actions'
        React.DOM.tbody null,
          # Iterate through all the records and create a new Record element for each one
          for record in @state.records
            React.createElement Record, key: record.id, record: record
