# Load the actions we just defined
RecordActions = require '../actions/RecordActions'

# Private variable to store all the records on the page
_records = []

# Create the actual store
RecordStore = Reflux.createStore(
  # All of these 'listenTo' calls are setting up this store to listen to the events we defined
  # previously, then indicate how to handle them when they're called. Notice that there are no
  # params defined here, but there are ones on the 'onCreate', etc. functions below. You'll see
  # why when we get to components.
  init: ->
    @listenTo(RecordActions.setRecords, @setRecords)
    @listenTo(RecordActions.createRecord, @onCreate)
    @listenTo(RecordActions.editRecord, @onEdit)
    @listenTo(RecordActions.deleteRecord, @onDelete)

  onCreate: (record) ->
    # When a record is created, push it into the array of other records.
    _records.push(record)
    # then trigger the view. This will send _records to anything listening to this store
    # (namely, the component(s)).
    @trigger(_records)

  # Anything that wants to edit a record needs to give us two things: the existing record and the
  # new data.
  onEdit: (record, data) ->
    # First, we find where the record is in the array of records
    index = _records.indexOf record
    # Then we update the records we're storing. This is using a React addon (which we enabled in
    # config/application.rb earlier).
    _records = React.addons.update(_records, { $splice: [[index, 1, data]] })
    # Update the component(s)
    @trigger(_records)

  onDelete: (record) ->
    # Find the record
    index = _records.indexOf record
    # Use the same addon but with a different result
    _records = React.addons.update _records, { $splice: [[index, 1]] }
    # Update the component(s)
    @trigger(_records)

  # We won't use this, but it's good to see the convention for getting all the records
  getRecords: ->
    _records

  # Get a single record
  getRecord: (id) ->
    records = _.filter _records, (r) -> r.id == id
    records[0]

  # We'll use this to set _records when the page is loaded for the first time
  setRecords: (records) ->
    _records = records
    @trigger(_records)
)

module.exports = RecordStore