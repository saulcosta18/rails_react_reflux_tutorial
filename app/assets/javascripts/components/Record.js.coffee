# app/assets/javascripts/components/Record.js.coffee

# Load the actions, which we'll use to update the view in a very clean manner
RecordActions = require '../actions/RecordActions'

# Notice that "window.Record" is not required here.
Record = React.createClass(
  # This is the initial state for any Record we create (the "edit" attribute is used to toggle the
  # view we use in the table between an editable one and a read only one).
  getInitialState: ->
    edit: false
    date: @props.record.date
    title: @props.record.title
    amount: @props.record.amount

  # This is a really strange function. But without it, we wouldn't be able to actually modify the
  # value of a form field. That's how strict frameworks like React are.
  handleChange: (e) ->
    # Create a temp state object
    newState = {}
    # Change it
    newState[e.target.name] = e.target.value
    # Change the state. The reason the first two lines are required is because we want this to be
    # dynamic and support all form fields (instead of having one for date, title, etc.).
    @setState newState

  # All these other "handle" functions are just called using "onClick" events (notice we're
  # passing in 'e' as the event). Search for this function and see where it's called below to get
  # a better sense of just how it's being used.
  handleEdit: (e) ->
    # Prevent the default link click event
    e.preventDefault()
    # Grab all the data
    data =
      title: React.findDOMNode(@refs.title).value
      date: React.findDOMNode(@refs.date).value
      amount: React.findDOMNode(@refs.amount).value
    # Submit a PUT request to the Rails server
    $.ajax
      method: 'PUT'
      url: "/records/#{ @props.record.id }"
      dataType: 'JSON'
      data:
        record: data
      success: (data) =>
        # Toggle this Record as not being edited
        @setState edit: false
        # Here's our first event! This gets blasted off as an action to the store, which passes it
        # to its 'onEdit' function. You wouldn't be able to do this without Reflux.
        RecordActions.editRecord @props.record, data

  handleDelete: (e) ->
    e.preventDefault()
    # Another AJAX request to the Rails server
    $.ajax
      method: 'DELETE'
      url: "records/#{ @props.record.id }"
      dataType: 'JSON'
      success: () =>
        # Another event
        RecordActions.deleteRecord(@props.record)

  # Again, search for this below and you'll see how this is being used
  editValid: ->
    @state.date && @state.title && @state.amount

  # Called when we want to transition this row in the table from read only to editable.
  handleToggle: (e) ->
    e.preventDefault()
    @setState edit: !@state.edit

  # Skip ahead to the 'render' statement and you'll see that this is one of two different ways of
  # viewing a Record. This one is the editable version. I really don't like all the boilerplate
  # code here, but I haven't learned enough to reduce this yet.
  recordForm: ->
    React.DOM.tr null,
      React.DOM.td null,
        React.DOM.input
          className: 'form-control'
          type: 'text'
          defaultValue: @props.record.date
          onChange: @handleChange
          name: 'date'
          ref: 'date'
      React.DOM.td null,
        React.DOM.input
          className: 'form-control'
          type: 'text'
          defaultValue: @props.record.title
          onChange: @handleChange
          name: 'title'
          ref: 'title'
      React.DOM.td null,
        React.DOM.input
          className: 'form-control'
          type: 'number'
          defaultValue: @props.record.amount
          onChange: @handleChange
          name: 'amount'
          ref: 'amount'
      React.DOM.td null,
        React.DOM.a
          className: 'btn btn-default'
          onClick: @handleEdit
          disabled: !@editValid()
          'Update'
        React.DOM.a
          className: 'btn btn-danger'
          onClick: @handleToggle
          'Cancel'

  # Read only way of viewing a Record
  recordRow: ->
    React.DOM.tr null,
      React.DOM.td null, @props.record.date
      React.DOM.td null, @props.record.title
      React.DOM.td null, amountFormat(@props.record.amount)
      React.DOM.td
        className: 'btn-group'
        React.DOM.a
          className: 'btn btn-default'
          onClick: @handleToggle
          'Edit'
        React.DOM.a
          className: 'btn btn-danger'
          onClick: @handleDelete
          'Delete'

  # Depending on if this Record is in edit mode, render one of two different views
  render: ->
    if @state.edit
      @recordForm()
    else
      @recordRow()
)

module.exports = Record