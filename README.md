# Rails + ReactJS + RefluxJS #

I couldn't find a good tutorial using these three things, so I cobbled together a couple different ones, namely [this one about Rails + ReactJS](https://www.airpair.com/reactjs/posts/reactjs-a-guide-for-rails-developers) and [this one about ReactJS + RefluxJS](http://www.sitepoint.com/creating-note-taking-app-react-flux) to learn what I needed. Almost all the code / commands come from one of these two tutorials... this tutorial is just putting them both together.

**NB: I'm just learning more about ReactJS and RefluxJS myself, so take this tutorial with a grain of salt...**

Here's a good diagram to keep in mind when thinking about React and Reflux (credit to [this page](https://github.com/ServiceStackApps/Chat-React)):

![Diagram](https://raw.githubusercontent.com/ServiceStack/Assets/master/img/livedemos/chat-react/reflux-diagram.png)

## Initial Q/A ##

**Aren't JS frameworks slow? What's so good about React?**

React uses what is a called a "reactive data binding system". This means that changes trigger other changes within the application. Think of this as a line of dominos: if you knock one over, all the others will fall over as well.

An example of this in this tutorial is when we add a record and the value of our account changes. We don't have to actually go out of our way and trigger the change, but because the account value is tied to the sum of all the records in the account, we can just update the *data* and the view handles the rest.

**Okay, React sounds good, so why do I need Reflux?**

[Facebook's page for react](http://facebook.github.io/react/) says it best: "Lots of people use React as the V in MVC". It's responsible for updating what the user sees. Do you need Reflux to use React? No, not at all. In fact when I went through the tutorials this tutorial is based on I did the React-only one first, then bolted on the Reflux stuff.

But when you start working with React you'll find that there are a lot of different things you need to keep in mind as you're writing code, even with the help of the data binding system. Reflux fixes this by adding the concept of "events".

Using only React, when something happens you are responsible for updating all the "components" (think of these as views) by updating the data. This gets messy very fast. Reflux adds the concept of "stores" (places to store your data) to help separate these ideas of working with the data and working with the view.

Using Reflux, when something happens you trigger an "action" which your "store" is waiting for and knows what to do with it when it hears it. We'll go over this with examples in the tutorial.

**Is Reflux the same as Flux? What's different?**

Reflux is inspired by Flux (which like React is created by Facebook). The biggest difference is that Reflux simplifies things a bit by removing these things called a "dispatcher". Dispatchers (from what I can tell) are the listening parts of a store in Reflux.

## Rails Application ##

Nothing fancy here, just create a Rails application:

~~~~
rails new accounts
cd accounts
~~~~

Then install `bootstrap-sass` as shown [here](https://github.com/twbs/bootstrap-sass) (make sure you do the JS steps as well!).

Now add the folling gems to your Gemfile:

* `'react-rails', '~> 1.0'` : This is, you guessed it, React itself.
* `'underscore-rails'` : I like underscore for filtering data
* `'browserify-rails', '~> 0.7'` : This will allow you to use NPM packages and `require('react')` statements

Then install React:

~~~~
rails g react:install
~~~~

This will create a folder in "app/assets/javascripts" called "components" and a JS file names "components.js". Also add `//= require jquery_ujs` to your "application.js" file (below `//= require jquery`).

Now move "components.js" to "app.js.coffee" (in the same folder) (since we'll be using Reflux, not just React). You'll need to update the `require` statements to CoffeeScript syntax as well (`# = require foo` instead of `//= require foo`)

Let's scaffold some Rails stuff to work with:

~~~~
rails g resource Record title date:date amount:float
~~~~

Then create your database and create the table for Records:

~~~~
rake db:create db:migrate
~~~~

Now create a few records using the Rails console:

~~~~
Record.create title: 'Record 1', date: Date.today, amount: 500
Record.create title: 'Record 2', date: Date.today, amount: -100
~~~~

Finally, start your Rails server.

This is a tutorial focused on the use of React / Reflux with Rails, not Rails by itself, so I'm just going to give you the code for the RecordsController. If you don't know what this is doing then you can find a lot better explinations elsewhere :)

```
#!ruby
# app/controllers/records_controller.rb

class RecordsController < ApplicationController
  def index
    @records = Record.all
    respond_to do |format|
      format.json do
        render json: @records
      end
      format.html
    end
  end

  def create
    @record = Record.new(record_params)

    if @record.save
      render json: @record
    else
      render json: @record.errors, status: :unprocessable_entity
    end
  end

  def update
    @record = Record.find(params[:id])
    if @record.update(record_params)
      render json: @record
    else
      render json: @record.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @record = Record.find(params[:id])
    @record.destroy
    head :no_content
  end

  private

  def record_params
    params.require(:record).permit(:title, :amount, :date)
  end
end
```

Okay, now let's create our only HTML (ish) view in the application:

~~~~
<%# app/views/records/index.html.erb %>

<%= react_component 'Records' %>
~~~~

That's it! All the HTML (actually, ERB) we'll use. This `react_component` helper is provided by the `react-rails` gem we installed earlier.

Okay, one last thing on the Rails server. Go into "config/application.rb" and add this to the end of your application config:

```
#!ruby
config.react.addons = true

config.browserify_rails.commandline_options = "-t coffeeify --extension=\".js.coffee\""
```

The first one will allow us to use some React addons and the second one supports the use of CoffeeScript with NPM.

You should now be able to go to "localhost:3000/records" and see your two records.

**If you've already started your server, restart it now.**

## NPM Setup ###

Put this in a file called "package.json" in the root of your Rails application:

~~~~
{
  "name": "Accounts",
  "dependencies" : {
    "browserify": "~> 6.3",
    "browserify-incremental": "^1.4.0",
    "coffeeify": "~> 0.6"
  },
  "license": "MIT",
  "engines": {
    "node": ">= 0.10"
  }
}
~~~~

This is used by NPM to install various Node modules. Then run `npm install` (might need sudo?) to install the packages.

You also need to install Reflux! The file is located [here](https://raw.githubusercontent.com/spoike/refluxjs/master/dist/reflux.min.js) and should be placed in "vendor/lib/assets/javascripts"


## Reflux Actions ##

Okay, time to get our hands dirty with Reflux! I think it's better to start with this rather than React.

The first thing you'll want to do is define all the possible actions your application could take. These are things that the components will "trigger" and the stores will be listening for and respond to by updating their data, which in turns updates what the user sees (via the compoent). So it really goes full circle.

Each of these actions are ones we'll use later in the components.

```
#!coffeescript
# app/assets/javascripts/actions/RecordActions.js.coffee

RecordActions = Reflux.createActions([
  'setRecords'
  'createRecord'
  'editRecord'
  'deleteRecord'
])

module.exports = RecordActions
```

Nothing too crazy here, one thing that might be strange to you is the `module.exports` at the end (if you've never worked with Node). Not 100% sure, but I think this is just used to define what is included when you `require` this file elsewhere.

## Reflux Store ##

Okay, time to setup a place to store our data. It's important to note that **this is just the data stored on the page**. Every time we perform an action we need to update data in two places: the database (this is done by submitting an AJAX request to the Rails server) and the page (this is done by updating the store).

Here's the code, and with comments to explain what's going on (rather than repeating the code to explain it):

```
#!coffeescript
# app/assets/javascripts/stores/RecordStore.js.coffee

# Load the actions we just defined
RecordActions = require '../actions/RecordActions'

# Private variable to store all the records on the page
_records = []

# Create the actual store
RecordStore = Reflux.createStore(
  # All of these 'listenTo' calls are setting up this store to listen to the events we defined
  # previously, then indicate how to handle them when they're called. Notice that there are no
  # params defined here, but there are ones on the 'onCreate', etc. functions below. You'll see
  # why when we get to components.
  init: ->
    @listenTo(RecordActions.setRecords, @setRecords)
    @listenTo(RecordActions.createRecord, @onCreate)
    @listenTo(RecordActions.editRecord, @onEdit)
    @listenTo(RecordActions.deleteRecord, @onDelete)

  onCreate: (record) ->
    # When a record is created, push it into the array of other records.
    _records.push(record)
    # then trigger the view. This will send _records to anything listening to this store
    # (namely, the component(s)).
    @trigger(_records)

  # Anything that wants to edit a record needs to give us two things: the existing record and the
  # new data.
  onEdit: (record, data) ->
    # First, we find where the record is in the array of records
    index = _records.indexOf record
    # Then we update the records we're storing. This is using a React addon (which we enabled in
    # config/application.rb earlier).
    _records = React.addons.update(_records, { $splice: [[index, 1, data]] })
    # Update the component(s)
    @trigger(_records)

  onDelete: (record) ->
    # Find the record
    index = _records.indexOf record
    # Use the same addon but with a different result
    _records = React.addons.update _records, { $splice: [[index, 1]] }
    # Update the component(s)
    @trigger(_records)

  # We won't use this, but it's good to see the convention for getting all the records
  getRecords: ->
    _records

  # Get a single record
  getRecord: (id) ->
    records = _.filter _records, (r) -> r.id == id
    records[0]

  # We'll use this to set _records when the page is loaded for the first time
  setRecords: (records) ->
    _records = records
    @trigger(_records)
)

module.exports = RecordStore
```

## React Components ##

Time to work on the last part of our application: the compoents. This is where React finally gets to come into play.

For starters, let's create a file with a quick helper function for working with the record values:

```
#!coffeescript
# app/assets/javascripts/utils.js.coffee

@amountFormat = (amount) ->
  "$#{Number(amount).toLocaleString()}"
```

Also include this file in "app.js.coffee".

We'll start with the mother-view: `Records`. This will manage the entire page, actually, and use three other views to get stuff done: `Record`, `RecordForm` and `AmountBox`.

Like before, I've commented in the code with explanations.

```
#!coffeescript
# app/assets/javascripts/components/Records.js.coffee

# Pull in all the building blocks, some of which we'll create after this file
AmountBox     = require './AmountBox'
Record        = require './Record'
RecordActions = require '../actions/RecordActions'
RecordForm    = require './RecordForm'
RecordStore   = require '../stores/RecordStore'

# Create the React class. Notice that I'm using "winodw.Records" here. This might not be correct,
# but it seemed to work really well with Rails and Reflux.
window.Records = React.createClass

  # In React, you'll see a lot of two things: state and props. This sets the default value for
  # state.
  getInitialState: ->
    records: []

  # And this sets the default value for props.
  getDefaultProps: ->
    records: []

  # Whatever you put here will be called when the page is first loaded
  componentDidMount: ->
    # Load the current data from the Rails server
    $.get '/records.json', (data) =>
      RecordActions.setRecords(data)
    # Start listening for changes triggered by the store, and when something changes, call the
    # 'onChange' function
    @unsubscribe = RecordStore.listen(@onChange)

  # When we're done, stop listening to the store
  componentWillUnmount: ->
    @unsubscribe()

  # Remember that 'trigger' function in RecordStore? This is where _records go. 'setState' is a
  # special function we use to change the state of the page. This is the ONLY correct way to
  # change the state of a page.
  onChange: (records) ->
    # 'records' are just '_records' from RecordStore
    @setState records: records

  # This will be used to calculate the 'credits' box at the top of the page
  credits: ->
    # Using underscore, filter our records to ones that have a positive amount
    credits = _.filter @state.records, (r) -> r.amount >= 0
    # Sum them
    _.reduce credits, ((s, r) -> s + r.amount ), 0

  # For the 'debits' box at the top of the page
  debits: ->
    # Using underscore, filter our records to ones that have a negative amount
    debits = _.filter @state.records, (r) -> r.amount < 0
    # Sum them
    _.reduce debits, ((s, r) -> s + r.amount ), 0

  # Another box we'll have at the top of the page
  balance: ->
    @debits() + @credits()


  # 'render' is what you actually see on the page
  render: ->
    # Create a <div> with a class of 'records', etc.
    React.DOM.div
      className: 'records'
      React.DOM.h2
        className: 'title'
        'Records'
      React.DOM.div
        className: 'row'
        # Here we're creating other React elements. We haven't written these yet, but these will
        # make more sense when we have. For now, take note of the params we're passing in:
        # 'type', 'amount' and 'text'.
        React.createElement AmountBox, type: 'success', amount: @credits(), text: 'Credits'
        React.createElement AmountBox, type: 'danger',  amount: @debits(), text: 'Debits'
        React.createElement AmountBox, type: 'info', amount: @balance(), text: 'Balance'
      # Don't let this one slip by you! This is an entire form that we'll define in RecordForm.
      React.createElement RecordForm
      React.DOM.hr null
      # Create a table to store the current records
      React.DOM.table
        className: 'table table-bordered'
        # All these 'nulls' could instead be 'className: "myClass"' for example
        React.DOM.thead null,
          React.DOM.tr null,
            React.DOM.th null, 'Date'
            React.DOM.th null, 'Title'
            React.DOM.th null, 'Amount'
            React.DOM.th null, 'Actions'
        React.DOM.tbody null,
          # Iterate through all the records and create a new Record element for each one
          for record in @state.records
            React.createElement Record, key: record.id, record: record
```

Okay, let's start from the bottom of that last component and work through the components it needs. For starters, here is the `Record` component:

```
#!coffeescript
# app/assets/javascripts/components/Record.js.coffee

# Load the actions, which we'll use to update the view in a very clean manner
RecordActions = require '../actions/RecordActions'

# Notice that "window.Record" is not required here.
Record = React.createClass(
  # This is the initial state for any Record we create (the "edit" attribute is used to toggle the
  # view we use in the table between an editable one and a read only one).
  getInitialState: ->
    edit: false
    date: @props.record.date
    title: @props.record.title
    amount: @props.record.amount

  # This is a really strange function. But without it, we wouldn't be able to actually modify the
  # value of a form field. That's how strict frameworks like React are.
  handleChange: (e) ->
    # Create a temp state object
    newState = {}
    # Change it
    newState[e.target.name] = e.target.value
    # Change the state. The reason the first two lines are required is because we want this to be
    # dynamic and support all form fields (instead of having one for date, title, etc.).
    @setState newState

  # All these other "handle" functions are just called using "onClick" events (notice we're
  # passing in 'e' as the event). Search for this function and see where it's called below to get
  # a better sense of just how it's being used.
  handleEdit: (e) ->
    # Prevent the default link click event
    e.preventDefault()
    # Grab all the data
    data =
      title: React.findDOMNode(@refs.title).value
      date: React.findDOMNode(@refs.date).value
      amount: React.findDOMNode(@refs.amount).value
    # Submit a PUT request to the Rails server
    $.ajax
      method: 'PUT'
      url: "/records/#{ @props.record.id }"
      dataType: 'JSON'
      data:
        record: data
      success: (data) =>
        # Toggle this Record as not being edited
        @setState edit: false
        # Here's our first event! This gets blasted off as an action to the store, which passes it
        # to its 'onEdit' function. You wouldn't be able to do this without Reflux.
        RecordActions.editRecord @props.record, data

  handleDelete: (e) ->
    e.preventDefault()
    # Another AJAX request to the Rails server
    $.ajax
      method: 'DELETE'
      url: "records/#{ @props.record.id }"
      dataType: 'JSON'
      success: () =>
        # Another event
        RecordActions.deleteRecord(@props.record)

  # Again, search for this below and you'll see how this is being used
  editValid: ->
    @state.date && @state.title && @state.amount

  # Called when we want to transition this row in the table from read only to editable.
  handleToggle: (e) ->
    e.preventDefault()
    @setState edit: !@state.edit

  # Skip ahead to the 'render' statement and you'll see that this is one of two different ways of
  # viewing a Record. This one is the editable version. I really don't like all the boilerplate
  # code here, but I haven't learned enough to reduce this yet.
  recordForm: ->
    React.DOM.tr null,
      React.DOM.td null,
        React.DOM.input
          className: 'form-control'
          type: 'text'
          defaultValue: @props.record.date
          onChange: @handleChange
          name: 'date'
          ref: 'date'
      React.DOM.td null,
        React.DOM.input
          className: 'form-control'
          type: 'text'
          defaultValue: @props.record.title
          onChange: @handleChange
          name: 'title'
          ref: 'title'
      React.DOM.td null,
        React.DOM.input
          className: 'form-control'
          type: 'number'
          defaultValue: @props.record.amount
          onChange: @handleChange
          name: 'amount'
          ref: 'amount'
      React.DOM.td null,
        React.DOM.a
          className: 'btn btn-default'
          onClick: @handleEdit
          disabled: !@editValid()
          'Update'
        React.DOM.a
          className: 'btn btn-danger'
          onClick: @handleToggle
          'Cancel'

  # Read only way of viewing a Record
  recordRow: ->
    React.DOM.tr null,
      React.DOM.td null, @props.record.date
      React.DOM.td null, @props.record.title
      React.DOM.td null, amountFormat(@props.record.amount)
      React.DOM.td
        className: 'btn-group'
        React.DOM.a
          className: 'btn btn-default'
          onClick: @handleToggle
          'Edit'
        React.DOM.a
          className: 'btn btn-danger'
          onClick: @handleDelete
          'Delete'

  # Depending on if this Record is in edit mode, render one of two different views
  render: ->
    if @state.edit
      @recordForm()
    else
      @recordRow()
)

module.exports = Record
```

Now we have a way of viewing records in the table and editing current ones, but we need a way to add new ones. We'll create another component called `RecordForm` for this.

```
#!coffeescript
# app/assets/javascripts/components/RecordForm.js.coffee

RecordActions = require '../actions/RecordActions'

RecordForm = React.createClass(
  # Notice that the state here is for the form fields, not an actual Record object
  getInitialState: ->
    title: ''
    date: ''
    amount: ''

  # Same as the one in Record. Could probably be moved to utils.
  handleChange: (e) ->
    newState = {}
    newState[e.target.name] = e.target.value
    @setState newState

  # Event called to create a new Record
  handleSubmit: (e) ->
    e.preventDefault()
    # Send the data to the Rails server. Notice that we're submitting @state as the data for the
    # new record!
    $.post '', { record: @state }, (data) =>
      # After it's done, use an action to update the store, and then the view
      RecordActions.createRecord(data)
      # Clear the form
      @setState @getInitialState()
    , 'JSON'

  valid: ->
    @state.date && @state.title && @state.amount

  # Another form
  render: ->
    React.DOM.form
      className: 'form-inline'
      onSubmit: @handleSubmit
      React.DOM.div
        className: 'form-group'
        React.DOM.input
          type: 'text'
          className: 'form-control'
          placeholder: 'Date'
          name: 'date'
          value: @state.date
          onChange: @handleChange
      React.DOM.div
        className: 'form-group'
        React.DOM.input
          type: 'text'
          className: 'form-control'
          placeholder: 'Title'
          name: 'title'
          value: @state.title
          onChange: @handleChange
      React.DOM.div
        className: 'form-group'
        React.DOM.input
          type: 'number'
          className: 'form-control'
          placeholder: 'Amount'
          name: 'amount'
          value: @state.amount
          onChange: @handleChange
      React.DOM.button
        type: 'submit'
        className: 'btn btn-primary'
        disabled: !@valid()
        'Create Record'
)

module.exports = RecordForm
```

One more to go! This component is used to render the balance boxes at the top of the page (remember there were three of them in Records?).

```
#!coffeescript
# app/assets/javascripts/components/AmountBox.js.coffee

AmountBox = React.createClass(
  render: ->
    React.DOM.div
      className: 'col-md-4'
      React.DOM.div
        # Remember what we passed in? It was for this class
        className: "panel panel-#{ @props.type }"
        React.DOM.div
          className: 'panel-heading'
          # this text
          @props.text
        React.DOM.div
          className: 'panel-body'
          # and this value (this is using the 'amountFormat' function from utils)
          amountFormat(@props.amount)
)

module.exports = AmountBox
```

Time to see where we're at! Head to your browser and navigate to "localhost:3000/records". If everything is ready to go, you should see an entire application now.