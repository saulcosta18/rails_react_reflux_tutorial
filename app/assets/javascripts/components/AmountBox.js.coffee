# app/assets/javascripts/components/AmountBox.js.coffee

AmountBox = React.createClass(
  render: ->
    React.DOM.div
      className: 'col-md-4'
      React.DOM.div
        # Remember what we passed in? It was for this class
        className: "panel panel-#{ @props.type }"
        React.DOM.div
          className: 'panel-heading'
          # this text
          @props.text
        React.DOM.div
          className: 'panel-body'
          # and this value (this is using the 'amountFormat' function from utils)
          amountFormat(@props.amount)
)

module.exports = AmountBox