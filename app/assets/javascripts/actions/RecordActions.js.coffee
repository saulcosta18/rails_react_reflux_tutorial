RecordActions = Reflux.createActions([
  'setRecords'
  'createRecord'
  'editRecord'
  'deleteRecord'
])

module.exports = RecordActions